#include <iostream>
#include <vector>
#include "Eigen/Core"
#include "Eigen/Eigenvalues"
#include "objloader.h"
#include "OpenGLSupport"

int main(int argc, const char * argv[]) {
    Eigen::Matrix3d m;
    m(0,0) = 1;
    m(0,1) = 1;
    m(0,2) = 0;
    m(1,0) = 0;
    m(1,1) = 2;
    m(1,2) = 0;
    m(2,0) = 0;
    m(2,1) = -1;
    m(2,2) = 4;
    Eigen::EigenSolver<Eigen::MatrixXd> es(m);
    std::cout << "The matrix" << std::endl << m << std::endl;
    std::cout << "has eigenvalues:" << std::endl << es.eigenvalues() << std::endl;
    
    std::cout << "Note some floating point error!" << std::endl  << std::endl;
    
    std::cout << "Press return to continue" << std::endl;
    getchar();
        
    std::vector<Eigen::Vector3d> vertices;
    std::vector<Eigen::Vector3d> faceIndices;
    
    // load the model.
    loadOBJ("bunny.obj", vertices, faceIndices);
    
    // print the first 5 lines of the model's vertices and faces
    std::cout << "Some bunny verticies:" << std::endl;
    for ( unsigned int i=0; i<5; i++ ){
        printf("%f %f %f\n",vertices[i](0),vertices[i](1),vertices[i](2));
    }
    std::cout << "..." << std::endl << "Press return for ALL bunny faces" << std::endl;
    getchar();
    
    for ( unsigned int i=0; i<faceIndices.size(); i++ ){
        printf("%d %d %d\n",int(faceIndices[i](0)),int(faceIndices[i](1)),int(faceIndices[i](2)));
    }
    return 0;
    
}
